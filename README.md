Unofficial Snap Package for puddletag

Install it using the snap store, gnome-software or using the command line:
https://snapcraft.io/puddletag-snap

Visit the official site:
https://docs.puddletag.net/index.html

https://github.com/puddletag/puddletag